## PoC of GitLab APIs useful for GitLab Releases

The initial focus of this PoC is for using the GitLab API to produce GitLab Releases where the metadata includes links to binaries that have been uploaded to GitLab directly.

Worthy reading: https://gitlab.com/groups/gitlab-org/-/epics/2202

Also this SO answer is influencing the PoC implementation: https://stackoverflow.com/a/55415383

## Storing Binaries on directly on GitLab

The two options identifed are Job Artifacts and the Project Upload API. A third option is discussed further below for storing binaries outside GitLab.

### Job Artifacts

https://docs.gitlab.com/ee/api/job_artifacts.html

This is part of the GitLab CI functionality as discussed here: https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html

They can be configured via the `.gitlab-ci.yml`. These uploaded artifacts will be kept in GitLab as defined by the `expire_in` config but can be kept forever. This can be further extracted using the rest api (https://gitlab_url/api/v4/projects/:id/jobs/artifacts/:ref_name/download?job=name), 

Artifacts can be most easily found under the job url where they were created.

For an example of using tagging/versioning see [.gitlab-ci.yml](.gitlab-ci.yml). The approach could potentially be part of the build and release process of collection tgz files as shown in the pipeline results.

**Observations**

Unfortunately, projects that are under the "Internal" Visiblity do not allow access to the REST API. 

A downside of Artifacts is that they were not really designed for releases (they are meant to pass artifacts between jobs in a pipeline). A related request can be seen [here](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/48736) and also [here](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/48736)

Another downside is that the solution has some bugs, such as this one raised here relating to tags that would be a good approach if the bug did not exist: https://gitlab.com/gitlab-org/gitlab/-/issues/235159

### Upload API

https://docs.gitlab.com/ee/api/projects.html#upload-a-file

See the PoC script [here](bin/release_with_upload.sh)

**Observations**

A downside of this approach is that there is no clear way to delete uploads. Suggestions for a workaround are to back up GitLab remove the binary from the back up and restore, which is clearly insufficient.

Related discussions:
* https://gitlab.com/gitlab-org/gitlab-foss/-/issues/23553
* https://gitlab.com/gitlab-org/gitlab-foss/-/issues/14256

## GitLab Releases

### Linking to externally hosted binaries

See the PoC script [here](bin/release_ova.sh) that shows a release on Gitlab being linked to an externally hosted binary on S3

### Release Management API (GitLab 11.7)

https://docs.gitlab.com/ee/api/releases/

### Release Links API (GitLab 11.7)

https://docs.gitlab.com/ee/api/releases/links.html

Useful for managing the links to binaries

### Release Evidences API (GitLab 12.5)

https://gitlab.com/gitlab-org/gitlab/-/issues/26019

## PoC Conclusions

There is no clean solution natively in GitLab for retaining binaries built in GitLab CI for release purposes, and whilst it is technically feasible, the support for this capability is disappointing.

It looks like the best approach would be to store binaries in something like S3 and just use external links associated with a release. This was clearly the case for large binaries such as OVA files, but for binaries such as Ansible collections, it would be nice to be able to build and store them all within GitLab itself as they are quite small.
