#!/bin/bash

set -euo pipefail
# tag_name=ova-$(date +%s)

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
S3_MINIO_BUCKET="sc-release-candidates"
OVA_BUILD_NO="0.0.1-build.1.ova"
OVA_FILE_NAME="sc-deployment-${OVA_BUILD_NO}"
tag_name=${OVA_BUILD_NO}

git tag -d "${tag_name}" || true
git tag "${tag_name}"
git push "${GIT_REMOTE_ALIAS}" --tags
body="{\"description\": \"PoC for an OVA release\"}"
curl -ks --request POST --header "PRIVATE-TOKEN: ${GITLAB_API_ACCESS_TOKEN}" \
	--header "Content-Type: application/json" \
	--data "${body}" \
	"${GITLAB_API_PROJECT_URL}"/repository/tags/"${tag_name}"/release \
	| jq .
curl -ks --request POST --header "PRIVATE-TOKEN: ${GITLAB_API_ACCESS_TOKEN}" \
        --data name="${OVA_FILE_NAME}" \
        --data url="${S3_MINIO_URL}/${S3_MINIO_BUCKET}/${OVA_FILE_NAME}" \
	"${GITLAB_API_PROJECT_URL}"/releases/"${tag_name}"/assets/links \
	| jq .
echo "Release complete. Getting release info.."
curl -ks --header "PRIVATE-TOKEN: ${GITLAB_API_ACCESS_TOKEN}" \
	--header "Content-Type: application/json" \
	"${GITLAB_API_PROJECT_URL}"/releases/"${tag_name}" \
	| jq .
