#!/bin/bash

set -euo pipefail
tag_name=upload-api-$(date +%s)

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

curl -skfLv --request POST --header "PRIVATE-TOKEN: ${GITLAB_API_ACCESS_TOKEN}" \
        --form "file=@${SCRIPT_DIR}/fake-collections.tgz" \
	"${GITLAB_API_PROJECT_URL}"/uploads
uploaded=$(curl -sk --request POST --header "PRIVATE-TOKEN: ${GITLAB_API_ACCESS_TOKEN}" \
        --form "file=@${SCRIPT_DIR}/fake-collections.tgz" \
	"${GITLAB_API_PROJECT_URL}"/uploads)
echo $uploaded | jq .
markdown=$(echo ${uploaded} | jq -r .markdown)
binary_url=${GITLAB_API_PROJECT_URL}$(echo ${uploaded} | jq -r .url)
git tag "${tag_name}"
git push "${GIT_REMOTE_ALIAS}" --tags
body="{\"description\": \"PoC Collections release. ${markdown}\"}"
curl -sk --request POST --header "PRIVATE-TOKEN: ${GITLAB_API_ACCESS_TOKEN}" \
	--header "Content-Type: application/json" \
	--data "${body}" \
	"${GITLAB_API_PROJECT_URL}"/repository/tags/"${tag_name}"/release \
	| jq .
curl -sk --request POST --header "PRIVATE-TOKEN: ${GITLAB_API_ACCESS_TOKEN}" \
        --data name="fake-collections.tgz" \
        --data url="${binary_url}" \
	"${GITLAB_API_PROJECT_URL}"/releases/"${tag_name}"/assets/links \
	| jq .
echo "Release complete. Getting release info.."
curl -sk --header "PRIVATE-TOKEN: ${GITLAB_API_ACCESS_TOKEN}" \
	--header "Content-Type: application/json" \
	"${GITLAB_API_PROJECT_URL}"/releases/"${tag_name}" \
	| jq .
